﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using EU4Lib.Entities;

namespace EU4_RWG.Builders
{
    internal class CultureBuilder
    {
        public void BuildCultureGroups()
        {
            G.CultureGroups = new List<CultureGroup>();
            G.Joins = new Joins();
            foreach (Region region in G.Map.Regions())
            {
                var cultureGroup = new CultureGroup
                {
                    Id = IdGenerator.GetNextCultureGroupId()
                };
                G.CultureGroups.Add(cultureGroup);
                // Pairs the culture group and region
                G.Joins.CultureGroupToRegionPairs.Add(
                    new Pair<CultureGroup, Region>(cultureGroup, region));
            }
            //TEST
        }

        public void CalculateCultureGroupOverlap()
        {
            
        }

        public void BuildCultures()
        {
            foreach (Area area in G.Map.Areas())
            {
                var cultureGroup = G.Joins.GetCultureGroup(area.ParentRegion);

                var culture = new Culture
                {
                    Id = IdGenerator.GetNextCultureId()
                };
                cultureGroup.Cultures.Add(culture);
                G.Joins.CultureToAreaPairs.Add(
                    new Pair<Culture, Area>(culture, area));
            }
            TestExpand();
        }

        public void TestExpand()
        {
            // For each culture:
            // Get the culture's area
            // Get the area's neighbors
            // Set the area's neghbors' culture relation to the loop culture
            // Then, finally, set the province cultures
            foreach (Culture culture in G.CultureGroups.SelectMany(x => x.Cultures))
            {
                var areas = G.Joins.GetAreas(culture);
                var areaNeighbors = areas.SelectMany(x => x.Neighbors).ToList();
                foreach (Area areaNeighbor in areaNeighbors)
                {
                    var join = G.Joins.CultureToAreaPairs.First(x => x.Item2.Equals(areaNeighbor));
                    join.Item1 = culture;
                }
                var allProvinces = new List<Province>();
                allProvinces.AddRange(areas.SelectMany(x => x.Provinces()));
                allProvinces.AddRange(areaNeighbors.SelectMany(x=> x.Provinces()));
                foreach (Province prov in allProvinces)
                {
                    prov.Culture = culture;
                }
            }
        }
    }
}
