﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using EU4Lib;
using EU4Lib.Entities;

namespace EU4_RWG
{
    class Program
    {
        private Program(string seed)
        {
            G.Seed = seed;
            G.Random = new Random(seed.GetHashCode());
        }

        private static void Main(string[] args)
        {
            string arg = ParseArguments(args);

            var p = new Program(arg);

            p.Run();
        }

        private void Run()
        {
            string eu4Directory = ConfigurationManager.AppSettings["Eu4Directory"];

            var eu4Deserializer = new Eu4Deserializer(eu4Directory);

            G.Map = eu4Deserializer.LoadMap();
            eu4Deserializer.LoadAllAdjacencies(G.Map);

            var generatorDirector = new Eu4RandomWorldGeneratorDirector();
            generatorDirector.ConstructWorld();
        }

        /// <summary>
        /// Parses the arguments. If none are found, a random seed is returned
        /// </summary>
        /// <param name="args">The arguments</param>
        private static string ParseArguments(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("No seed found! Generating random seed...");
                string randomSeed = GetRandomSeed();
                Console.WriteLine($"Random Seed: {randomSeed}");
                return randomSeed;
            }
            else
            {
                return args.Length > 1
                    ? string.Join(" ", args)
                    : args[0];
            }
        }

        /// <summary>
        /// Generates a random seed
        /// </summary>
        /// <returns>A random seed</returns>
        private static string GetRandomSeed()
        {
            var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var seedBytes = new byte[16];
            rngCryptoServiceProvider.GetBytes(seedBytes);

            string randomSeed = BitConverter.ToString(seedBytes).Replace("-", "");
            return randomSeed;
        }
    }
}
