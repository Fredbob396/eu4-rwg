﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EU4Lib.Entities;
using EU4_RWG.Builders;

namespace EU4_RWG
{
    /// <summary>
    /// Generates the entire world
    /// </summary>
    internal class Eu4RandomWorldGeneratorDirector
    {
        public void ConstructWorld()
        {
            var culturebuilder = new CultureBuilder();

            culturebuilder.BuildCultureGroups();
            culturebuilder.BuildCultures();
        }
    }
}
