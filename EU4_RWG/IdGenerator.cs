﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4_RWG
{
    class IdGenerator
    {
        private static int _cultureGroupCount;
        public static string GetNextCultureGroupId()
        {
            string internalName = $"culturegroup_{_cultureGroupCount:000000}";
            _cultureGroupCount++;
            return internalName;
        }

        private static int _cultureCount;
        public static string GetNextCultureId()
        {
            string internalName = $"culture_{_cultureCount:000000}";
            _cultureCount++;
            return internalName;
        }

        private static int _religionGroupCount;
        public static string GetNextReligionGroupId()
        {
            string internalName = $"religiongroup_{_religionGroupCount:000000}";
            _religionGroupCount++;
            return internalName;
        }

        private static int _religionCount;
        public static string GetNextReligionId()
        {
            string internalName = $"religion_{_religionCount:000000}";
            _religionCount++;
            return internalName;
        }

        private static int _hereticCount;
        public static string GetNextHereticId()
        {
            string internalName = $"heretic_{_hereticCount:000000}";
            _hereticCount++;
            return internalName;
        }
    }
}
