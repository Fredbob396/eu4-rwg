﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EU4Lib.Entities;

namespace EU4_RWG
{
    /// <summary>
    /// Globals
    /// </summary>
    internal static class G
    {
        internal static string Seed { get; set; }
        internal static Random Random { get; set; }
        internal static Map Map { get; set; }
        internal static List<CultureGroup> CultureGroups { get; set; }
        internal static Joins Joins { get; set; }
    }
}
