﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EU4Lib.Entities;

namespace EU4_RWG
{
    public class Joins
    {
        /// <summary>
        /// Helps to determine a culture's culture group
        /// </summary>
        public List<Pair<CultureGroup, Region>> CultureGroupToRegionPairs = new List<Pair<CultureGroup, Region>>();

        /// <summary>
        /// Helps to determine a culture's starting province
        /// </summary>
        public List<Pair<Culture, Area>> CultureToAreaPairs = new List<Pair<Culture, Area>>();

        public List<Region> GetRegions(CultureGroup cultureGroup)
        {
            var pairs = CultureGroupToRegionPairs.Where(x => x.Item1.Equals(cultureGroup)).ToList();
            return pairs.Any()
                ? pairs.Select(x => x.Item2).ToList()
                : null; // Returns null if the pair in null
        }

        public CultureGroup GetCultureGroup(Region region)
        {
            var pair = CultureGroupToRegionPairs.FirstOrDefault(x => x.Item2.Equals(region));
            return pair?.Item1;
        }

        public List<Area> GetAreas(Culture culture)
        {
            var pairs = CultureToAreaPairs.Where(x => x.Item1.Equals(culture)).ToList();
            return pairs.Any()
                ? pairs.Select(x => x.Item2).ToList()
                : null; // Returns null if the pair in null
        }

        public Culture GetCulture(Area area)
        {
            var pair = CultureToAreaPairs.FirstOrDefault(x => x.Item2.Equals(area));
            return pair?.Item1;
        }
    }
}
