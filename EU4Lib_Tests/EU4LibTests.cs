﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EU4Lib;
using EU4Lib.Entities;

// To Debug, press CTRL+R and then CTRL+T
namespace EU4Lib_Tests
{
    [TestClass]
    public class EU4LibTests
    {
        private const string Eu4Directory = @"E:/SteamLibrary/SteamApps/common/Europa Universalis IV";

        [TestMethod]
        public void MapDeserializersLoadsAtLeastOneSuperregion()
        {
            var deserializer = new Eu4Deserializer(Eu4Directory);
            Map map = deserializer.LoadMap();

            Assert.IsTrue(map.SuperRegions().Count > 0);
        }

        [TestMethod]
        public void LoadAllAdjacencies()
        {
            var deserializer = new Eu4Deserializer(Eu4Directory);
            Map map = deserializer.LoadMap();
            deserializer.LoadAllAdjacencies(map);

            Assert.IsTrue(map.Provinces()[0].Neighbors.Count > 0);
        }

        //[TestMethod]
        //public void BuildAdjacenciesDatabase()
        //{
        //    var deserializer = new Eu4Deserializer(Eu4Directory);
        //    Map map = deserializer.LoadMap();
        //    deserializer.BuildAdjacenciesDatabase(map);

        //    Assert.IsTrue(map.GetProvinces()[0].Neighbors.Count > 0);
        //}
    }
}
