﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EU4Lib.Entities;

namespace EU4Lib.MapService
{
    public class MapDeserializer
    {
        private readonly string _areaFile = $@"{G.Eu4Directory}/map/area.txt";
        private readonly string _regionFile = $@"{G.Eu4Directory}/map/region.txt";
        private readonly string _superregionFile = $@"{G.Eu4Directory}/map/superregion.txt";
        private readonly string _defaultMapFile = $@"{G.Eu4Directory}/map/default.map";

        private readonly List<Area> _areas = new List<Area>();
        private readonly List<Region> _regions = new List<Region>();
        private readonly List<SuperRegion> _superregions = new List<SuperRegion>();
        private readonly List<Province> _provinces = new List<Province>();
        private Map _map;

        public Map GetDeserializedMap()
        {
            LoadAreas();
            LoadRegions();
            LoadSuperregions();
            BuildRelations();
            PurgeEmptySuperregions();

            _map = new Map(_superregions);

            GetSeaProvinces();

            return _map;
        }

        private void LoadAreas()
        {
            using (var reader = new StreamReader(File.OpenRead(_areaFile)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    line = RemoveComments(line);

                    //If the resulting line is blank or is a color node, skip the line
                    if (string.IsNullOrWhiteSpace(line) || line.Contains("color ="))
                    {
                        continue;
                    }

                    //Reads each line until a line with ONLY a closing brace is found
                    if (line.Contains("{") && !line.Contains("}"))
                    {
                        var newArea = new Area()
                        {
                            Name = TrimName(line)
                        };
                        _areas.Add(newArea);

                        //Continues to search for provinces until some are found.
                        while (line != "}")
                        {
                            line = reader.ReadLine();
                            line = RemoveComments(line);
                            if (string.IsNullOrWhiteSpace(line) || line.Contains("color ="))
                            {
                                continue;
                            }

                            //If the line contains a closing brace, end the node
                            if (line.Contains("}"))
                            {
                                break;
                            }

                            //Removes extra spaces, I think
                            line = Regex.Replace(line, @"\s+", " ");

                            //Splits out the string of provinces and adds them to the area
                            string[] provinceArray = line.Split(' ');
                            foreach (string s in provinceArray)
                            {
                                _provinces.Add(new Province()
                                {
                                    Id = Convert.ToInt32(s)
                                });
                            }
                            newArea.ProvinceIds.AddRange(provinceArray);
                        }
                    }
                }
            }
        }

        private void LoadRegions()
        {
            using (var reader = new StreamReader(File.OpenRead(_regionFile)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    //Strip all comments from the line
                    line = RemoveComments(line);

                    //If the resulting line is blank, skip the line
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    //If the line has an open brace, that means that it is the start of a new region node.
                    // Ignore monsoon blocks
                    if (line.Contains("{") && !line.Contains("monsoon"))
                    {
                        //Trims other characters from the region name and creates a new area
                        var newRegion = new Region()
                        {
                            Name = TrimName(line)
                        };
                        _regions.Add(newRegion);

                        //Reads each line until a line with ONLY a closing brace is found
                        //This also ignores lines that have a brace plus whitespace, which excludes
                        //The closing bracket of the "areas" node.
                        while (line != "}")
                        {
                            line = reader.ReadLine();
                            line = RemoveComments(line);
                            //If the line is the areas node or contains a closing brace or is blank, skip it
                            if (line.Contains("areas") || line.Contains("}") || string.IsNullOrWhiteSpace(line))
                            {
                                continue;
                            }
                            newRegion.AreaNames.Add(line.Trim());
                        }
                    }
                }
            }
        }

        private void LoadSuperregions()
        {
            using (var reader = new StreamReader(File.OpenRead(_superregionFile)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    //Strip all comments from the line
                    line = RemoveComments(line);

                    //If the resulting line is blank, skip the line
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    //If the line has an open brace, that means that it is the start of a new region node.
                    if (line.Contains("{"))
                    {
                        //Trims other characters from the superregion name and creates a new area
                        var newSuperRegion = new SuperRegion()
                        {
                            Name = TrimName(line)
                        };
                        _superregions.Add(newSuperRegion);

                        //Reads each line until a line with ONLY a closing brace is found
                        //This also ignores lines that have a brace plus whitespace
                        while (line != "}")
                        {
                            line = reader.ReadLine();
                            line = RemoveComments(line);
                            //If the line contains a closing brace or is blank, skip it
                            if (line.Contains("}") || string.IsNullOrWhiteSpace(line))
                            {
                                continue;
                            }
                            newSuperRegion.RegionNames.Add(line.Trim());
                        }
                    }
                }
            }
        }

        private void GetSeaProvinces()
        {
            var seaStarts = new List<string>();
            string seaStartsBlock = "";
            using (var reader = new StreamReader(File.OpenRead(_defaultMapFile)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    line = RemoveComments(line);
                    if (line.Contains("width") ||
                        line.Contains("height") ||
                        line.Contains("max_provinces") ||
                        string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }
                    if (line.Contains("sea_starts"))
                    {
                        while (!line.Contains("}"))
                        {
                            line = reader.ReadLine();
                            line = RemoveComments(line);
                            if (!line.Contains("}"))
                            {
                                seaStartsBlock += line + " ";
                            }
                        }
                        seaStartsBlock = seaStartsBlock.Replace("\t", " ");
                        seaStarts = seaStartsBlock.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToList();
                        break;
                    }
                }

                foreach (string seaStart in seaStarts)
                {
                    var seaProvince = _map.Provinces().FirstOrDefault(x => x.Id == int.Parse(seaStart));
                    if(seaProvince == null)
                        continue;
                    seaProvince.Sea = true;
                    seaProvince.ParentArea.Sea = true;
                    seaProvince.ParentRegion.Sea = true;
                    seaProvince.ParentSuperRegion.Sea = true;
                }
            }
        }

        private void BuildRelations()
        {
            // Matches regions to their parent superregions
            foreach (SuperRegion superRegion in _superregions)
            {
                foreach (string regionName in superRegion.RegionNames)
                {
                    Region childRegion = _regions.First(r => r.Name.Equals(regionName));
                    if (childRegion.ParentSuperRegion != null)
                    {
                        Debug.WriteLine($"WARNING: {superRegion.Name} has duplicate child {childRegion.Name}, which belongs to {childRegion.ParentSuperRegion.Name}! Ignoring");
                        continue;
                    }
                    superRegion.Regions().Add(childRegion);
                    childRegion.ParentSuperRegion = superRegion;
                }
                superRegion.RegionNames = null;
            }
            // Matches areas to their parent regions
            foreach (Region region in _regions)
            {
                foreach (string areaName in region.AreaNames)
                {
                    Area childArea = _areas.First(a => a.Name.Equals(areaName));
                    if (childArea.ParentRegion != null)
                    {
                        Debug.WriteLine($"WARNING: {region.Name} has duplicate child {childArea.Name}, which belongs to {childArea.ParentRegion.Name}! Ignoring");
                        continue;
                    }
                    region.Areas().Add(childArea);
                    childArea.ParentRegion = region;
                    childArea.ParentSuperRegion = region.ParentSuperRegion;
                }
                region.AreaNames = null;
            }
            // Matches provinces to their parent areas
            foreach (Area area in _areas)
            {
                foreach (string provinceId in area.ProvinceIds)
                {
                    Province childProvince = _provinces.First(p => p.Id.Equals(Convert.ToInt32(provinceId)));
                    if (childProvince.ParentArea != null)
                    {
                        Debug.WriteLine($"WARNING: {area.Name} has duplicate child {childProvince.Id}, which belongs to {childProvince.ParentArea.Name}! Ignoring");
                        continue;
                    }
                    area.Provinces().Add(childProvince);
                    childProvince.ParentArea = area;
                    childProvince.ParentRegion = area.ParentRegion;
                    childProvince.ParentSuperRegion = area.ParentSuperRegion;
                }
                area.ProvinceIds = null;
            }
        }

        private void PurgeEmptySuperregions()
        {
            _superregions.RemoveAll(sr => sr.Regions().Count == 0);
        }

        private string TrimName(string line)
        {
            int index = line.IndexOf("=", StringComparison.Ordinal);
            if (index > 0)
            {
                line = line.Substring(0, index);
            }
            line = line.Trim();

            // In case the equals sign is missing somehow
            index = line.IndexOf("{", StringComparison.Ordinal);
            if (index > 0)
            {
                line = line.Substring(0, index);
            }
            line = line.Trim();

            return line;
        }

        private string RemoveComments(string line)
        {
            int index = line.IndexOf('#');
            if (index > -1)
            {
                line = line.Substring(0, index);
            }
            line = line.Trim();
            return line;
        }
    }
}
