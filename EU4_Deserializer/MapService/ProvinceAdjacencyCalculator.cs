﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Text;
using EU4Lib.Entities;
using Point = EU4Lib.Entities.Point;
using Region = EU4Lib.Entities.Region;

namespace EU4Lib.MapService
{
    class ProvinceAdjacencyCalculator
    {
        private readonly Map _map;
        private DirectBitmap _mapImage;
        private Dictionary<Tuple<int, int>, Point> _mapPoints { get; set; }
        private List<Definition> _definition { get; set; }

        public ProvinceAdjacencyCalculator(Map map)
        {
            _map = map;
        }

        private readonly Dictionary<Point, Point> _clockwiseOffset = new Dictionary<Point, Point>()
        {
            { new Point { X = 1, Y = 0 }, new Point { X = 1, Y = -1 } },     // right        => down-right
            { new Point { X = 1, Y = -1 }, new Point { X = 0, Y = -1 } },    // down-right   => down
            { new Point { X = 0, Y = -1 }, new Point { X = -1, Y = -1 } },   // down         => down-left
            { new Point { X = -1, Y = -1 }, new Point { X = -1, Y = 0 } },   // down-left    => left
            { new Point { X = -1, Y = 0 }, new Point { X = -1, Y = 1 } },    // left         => top-left
            { new Point { X = -1, Y = 1 }, new Point { X = 0, Y = 1 } },     // top-left     => top
            { new Point { X = 0, Y = 1 }, new Point { X = 1, Y = 1 } },      // top          => top-right
            { new Point { X = 1, Y = 1 }, new Point { X = 1, Y = 0 } }       // top-right    => right
        };

        public void BuildAdjacenciesDatabase()
        {
            Debug.WriteLine("CreateAdjacenciesDatabase");
            CreateAdjacenciesDatabase();

            Debug.WriteLine("LoadDefinition");
            _definition = LoadDefinition();

            Debug.WriteLine("GetMapPoints");
            _mapPoints = GetMapPoints();

            Debug.WriteLine("GetDefinitionData");
            GetDefinitionData();

            Debug.WriteLine("GetAllProvinceNeighbors");
            GetAllProvinceNeighbors();

            Debug.WriteLine("DumpNeighborsToDatabase");
            DumpNeighborsToDatabase();
        }

        public void LoadAllAdjacencies()
        {
            LoadProvinceNeighbors();
            BuildAreaNeighbors();
            BuildRegionNeighbors();
            BuildSuperRegionNeighbors();
        }

        private void LoadProvinceNeighbors()
        {
            // Load neighbor info from DB
            var provAdjacenciesTable = new List<Tuple<int, int>>();
            using (var conn = new SQLiteConnection("Data Source=Adjacencies.db"))
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = "SELECT * FROM ProvinceAdjacencies";
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            provAdjacenciesTable.Add(new Tuple<int, int>(reader.GetInt32(0), reader.GetInt32(1)));
                        }
                }
            }

            // Using neighbor info, add neighbors to each other's lists
            var provinces = _map.Provinces();
            var adjGrouping = provAdjacenciesTable.GroupBy(x => x.Item1);
            foreach (var adjDict in adjGrouping)
            {
                var province = provinces.FirstOrDefault(x => x.Id == adjDict.Key);
                var neighbors = new List<Province>();
                if (province != null)
                {
                    foreach (Tuple<int, int> adjGroup in adjDict.ToList())
                    {
                        var neighbor = provinces.FirstOrDefault(x => x.Id == adjGroup.Item2);
                        if (neighbor != null)
                        {
                            neighbors.Add(neighbor);
                        }
                    }
                    province.Neighbors = neighbors;
                }
            }
        }

        private void BuildAreaNeighbors()
        {
            var areas = _map.Areas();
            foreach (Area area in areas)
            {
                var areaNeighbors = area.Provinces()
                    .SelectMany(x => x.Neighbors)
                    .Select(x => x.ParentArea)
                    .Where(x => !x.Equals(area))
                    .Distinct()
                    .ToList();

                area.Neighbors = areaNeighbors;
            }
        }

        private void BuildRegionNeighbors()
        {
            var regions = _map.Regions();
            foreach (Region region in regions)
            {
                var regionNeighbors = region.Areas()
                    .SelectMany(x => x.Neighbors)
                    .Select(x => x.ParentRegion)
                    .Where(x => !x.Equals(region))
                    .Distinct()
                    .ToList();

                region.Neighbors = regionNeighbors;
            }
        }

        private void BuildSuperRegionNeighbors()
        {
            var superRegions = _map.SuperRegions();
            foreach (SuperRegion superRegion in superRegions)
            {
                var regionNeighbors = superRegion.Regions()
                    .SelectMany(x => x.Neighbors)
                    .Select(x => x.ParentSuperRegion)
                    .Where(x => !x.Equals(superRegion))
                    .Distinct()
                    .ToList();

                superRegion.Neighbors = regionNeighbors;
            }
        }

        private void CreateAdjacenciesDatabase()
        {
            // Create the db file
            SQLiteConnection.CreateFile("Adjacencies.db");
            using (var conn = new SQLiteConnection("Data Source=Adjacencies.db"))
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(conn))
                using (var transaction = conn.BeginTransaction())
                {
                    // Create the Provinces table
                    cmd.CommandText = "CREATE TABLE `Provinces` ( `Id` INTEGER, `Name` TEXT, PRIMARY KEY(`Id`) )";
                    cmd.ExecuteNonQuery();

                    // Create the ProvincesAdjacencies table
                    cmd.CommandText = "CREATE TABLE `ProvinceAdjacencies` ( `ProvinceId` INTEGER, `NeighborId` INTEGER, PRIMARY KEY(`ProvinceId`,`NeighborId`) )";
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                }
            }
        }

        private List<Definition> LoadDefinition()
        {
            var definition = new List<Definition>();
            var definitionFile = File.ReadAllLines($@"{G.Eu4Directory}/map/definition.csv", Encoding.Default);
            bool first = true;
            foreach (string line in definitionFile)
            {
                // Skip header
                if (first) { first = false; continue; }

                var def = line.Split(';');
                definition.Add(new Definition
                {
                    ProvinceId = int.Parse(def[0]),
                    Color = Color.FromArgb(255, int.Parse(def[1]), int.Parse(def[2]), int.Parse(def[3])),
                    ProvinceName = def[4]
                });
            }
            return definition;
        }

        // TODO: Very slow since adding Tuple. Find a way to speed it up?
        private Dictionary<Tuple<int, int>, Point> GetMapPoints()
        {
            var pixels = new Dictionary<Tuple<int, int>, Point>();
            using (var tempBitmap = new Bitmap($@"{G.Eu4Directory}/map/provinces.bmp"))
            {
                _mapImage = new DirectBitmap(tempBitmap.Width, tempBitmap.Height);

                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    for (int x = 0; x < tempBitmap.Width; x++)
                    {
                        var color = tempBitmap.GetPixel(x, y);
                        _mapImage.SetPixel(x, y, color);
                        pixels.Add(Tuple.Create(x, y), new Point { X = x, Y = y, Color = color });
                    }
                }
            }

            return pixels;
        }

        private void GetDefinitionData()
        {
            var colorGroupings = _mapPoints.GroupBy(x => x.Value.Color).ToList();

            foreach (Definition definition in _definition)
            {
                // Try to get all points matching the definition color from the map image
                var defPoints = colorGroupings.FirstOrDefault(x => x.Key.Equals(definition.Color));
                var mapPoints = new Dictionary<Tuple<int, int>, Point>();
                if (defPoints == null)
                {
                    continue;
                }

                // Try get the province that matches the definition's ID
                var province = _map.Provinces().FirstOrDefault(x => x.Id == definition.ProvinceId);
                if (province == null)
                {
                    continue;
                }

                province.DefinitionName = definition.ProvinceName;
                province.Color = definition.Color;

                foreach (var keyValuePair in defPoints)
                {
                    mapPoints.Add(keyValuePair.Key, keyValuePair.Value);
                }

                province.MapPoints = mapPoints;
            }
        }

        private void GetAllProvinceNeighbors()
        {
            foreach (Province province in _map.Provinces().OrderBy(x => x.Id))
            {
                Debug.WriteLine($"Province #{province.Id} Start...");
                province.Neighbors = GetProvinceNeighbors(province);
                Debug.WriteLine($"Done!");
            }
        }

        private void DumpNeighborsToDatabase()
        {
            using (var conn = new SQLiteConnection("Data Source=Adjacencies.db"))
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(conn))
                using (var transaction = conn.BeginTransaction())
                {
                    foreach (var province in _map.Provinces())
                    {
                        try
                        {
                            // First insert regular province...
                            cmd.CommandText = "INSERT INTO Provinces VALUES(@ID, @NAME)";
                            cmd.Parameters.AddWithValue("@ID", province.Id);
                            cmd.Parameters.AddWithValue("@NAME", province.DefinitionName);
                            cmd.ExecuteNonQuery();

                            cmd.Parameters.Clear();

                            // Then insert all it's neighbors
                            cmd.CommandText = "INSERT INTO ProvinceAdjacencies VALUES(@ID, @NEIGHBOR)";
                            foreach (var neighbor in province.Neighbors)
                            {
                                cmd.Parameters.AddWithValue("@ID", province.Id);
                                cmd.Parameters.AddWithValue("@NEIGHBOR", neighbor.Id);
                                cmd.ExecuteNonQuery();

                                cmd.Parameters.Clear();
                            }
                        }
                        catch (Exception ex)
                        {
                            /* 
                             * Because the adjacency calculation process takes upwards of 
                             * 30 minutes, Every possible exception is caught.
                            */
                            Debug.WriteLine(ex.Message);
                        }
                    }
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Gets all province neighbors using the Moore Neighboorhood tracing algorithm
        /// https://github.com/Dkendal/Moore-Neighbor_Contour_Tracer/blob/master/ContourTrace.cs
        /// https://en.wikipedia.org/wiki/Moore_neighborhood#Algorithm
        /// </summary>
        private List<Province> GetProvinceNeighbors(Province province)
        {
            //TestConsoleBuffer();
            var outline = new List<Point>();
            Color targetColor = province.Color;
            List<List<Point>> chunks = GetProvinceChunks(province);

            foreach (List<Point> chunk in chunks)
            {
                Point beforeFirstPixel = null;
                Point firstPixel = null;
                Point boundaryPixel;
                Point currentPixel;
                Point backtrackPixel;
                // Find First of color
                for (int y = _mapImage.Height - 1; y >= 0; y--)
                {
                    beforeFirstPixel = new Point { X = 0, Y = y - 1 };
                    for (int x = 0; x < _mapImage.Width; x++)
                    {
                        // If color matches province, move on
                        if (_mapImage.GetPixel(x, y) == province.Color)
                        {

                            firstPixel = new Point { X = x, Y = y };

                            // Easiest way to break out this loop. Meh
                            goto FoundFirstPixel;
                        }
                        beforeFirstPixel = new Point { X = x, Y = y };
                    }
                }

                FoundFirstPixel:
                outline.Add(firstPixel);
                boundaryPixel = firstPixel;
                backtrackPixel = beforeFirstPixel;
                currentPixel = Clockwise(backtrackPixel, boundaryPixel);

                while (!currentPixel.ToString().Equals(firstPixel.ToString()))
                {
                    if (currentPixel.Y >= 0 &&
                        currentPixel.X >= 0 &&
                        currentPixel.Y < _mapImage.Height &&
                        currentPixel.X < _mapImage.Width &&
                        _mapImage.GetPixel(currentPixel.X, currentPixel.Y) == targetColor)
                    {
                        backtrackPixel = boundaryPixel;
                        boundaryPixel = currentPixel;
                        currentPixel = Clockwise(backtrackPixel, boundaryPixel);
                    }
                    else
                    {
                        outline.Add(currentPixel);
                        //if (currentPixel.X > -1 && currentPixel.Y > -1)
                        //{
                        //    Console.SetCursorPosition(currentPixel.X, currentPixel.Y);
                        //    Console.Write('X');
                        //}
                        backtrackPixel = currentPixel;
                        currentPixel = Clockwise(backtrackPixel, boundaryPixel);
                    }
                }

                // Change chunk color to white so it doesn't get searched again
                foreach (Point point in chunk)
                {
                    _mapImage.SetPixel(point.X, point.Y, Color.White);
                }
            }

            // Recolors image
            foreach (Point point in chunks.SelectMany(x => x))
            {
                _mapImage.SetPixel(point.X, point.Y, targetColor);
            }

            // Purge non-existent points
            outline = outline.Where(o => o.X > -1 &&
                                         o.Y > -1 &&
                                         o.X < _mapImage.Width &&
                                         o.Y < _mapImage.Height).ToList();

            // Get outline colors from image
            foreach (Point point in outline)
            {
                point.Color = _mapImage.GetPixel(point.X, point.Y);
            }

            List<Color> uniqueColors = outline
                .GroupBy(x => x.Color)
                .Select(x => x.Key)
                .Where(x => x != targetColor)
                .Distinct().ToList();

            return _map.Provinces().Where(x => uniqueColors.Contains(x.Color)).ToList();
        }

        private void TestConsoleBuffer()
        {
            Console.BufferHeight = 1000;
            Console.BufferWidth = 1000;
        }

        /// <summary>
        /// Gets the point Clockwise to the current point
        /// [prev - target] + target;
        /// </summary>
        private Point Clockwise(Point backtrack, Point boundary)
        {
            var offsetPoint = new Point
            {
                X = backtrack.X - boundary.X,
                Y = backtrack.Y - boundary.Y
            };

            // Gets the value from the dictionary using LINQ
            Point offset = _clockwiseOffset
                .First(x => x.Key.X == offsetPoint.X &&
                            x.Key.Y == offsetPoint.Y).Value;

            return new Point()
            {
                X = offset.X + boundary.X,
                Y = offset.Y + boundary.Y
            };
        }

        /// <summary>
        /// https://simpledevcode.wordpress.com/2015/12/29/flood-fill-algorithm-using-c-net/
        /// TODO: Is 4-directions a problem?
        /// TODO: http://will.thimbleby.net/scanline-flood-fill/ ???
        /// </summary>
        public List<List<Point>> GetProvinceChunks(Province province)
        {
            var chunks = new List<List<Point>>();
            var tempPoints = new List<Point>();
            int totalPixels = 0;

            tempPoints.AddRange(province.MapPoints.Values.ToList());

            while (totalPixels < province.MapPoints.Count)
            {
                var targetPixel = tempPoints.First();
                var chunk = GetChunk(targetPixel, province.Color);
                chunks.Add(chunk);
                totalPixels += chunk.Count;

                tempPoints = tempPoints.Except(chunk).ToList();
            }

            // Resets color of chunks after fill
            foreach (var point in chunks.SelectMany(x => x))
            {
                _mapImage.SetPixel(point.X, point.Y, province.Color);
            }

            return chunks;
        }

        public List<Point> GetChunk(Point point, Color targetColor)
        {
            var replacementColor = Color.White;
            var chunk = new List<Point>();

            var pixels = new Stack<Point>();

            pixels.Push(point);
            while (pixels.Count != 0)
            {
                Point temp = pixels.Pop();
                int y1 = temp.Y;
                while (y1 >= 0 && _mapImage.GetPixel(temp.X, y1) == targetColor)
                {
                    y1--;
                }
                y1++;
                bool spanLeft = false;
                bool spanRight = false;
                while (y1 < _mapImage.Height && _mapImage.GetPixel(temp.X, y1) == targetColor)
                {
                    var chunkPoint = TryGetPoint(temp.X, y1);
                    chunk.Add(chunkPoint);
                    _mapImage.SetPixel(temp.X, y1, replacementColor);

                    if (!spanLeft && temp.X > 0 && _mapImage.GetPixel(temp.X - 1, y1) == targetColor)
                    {
                        pixels.Push(new Point { X = temp.X - 1, Y = y1 });
                        spanLeft = true;
                    }
                    else if (spanLeft && temp.X - 1 >= 0 && _mapImage.GetPixel(temp.X - 1, y1) != targetColor)
                    {
                        spanLeft = false;
                    }
                    if (!spanRight && temp.X < _mapImage.Width - 1 && _mapImage.GetPixel(temp.X + 1, y1) == targetColor)
                    {
                        pixels.Push(new Point { X = temp.X + 1, Y = y1 });
                        spanRight = true;
                    }
                    else if (spanRight && temp.X < _mapImage.Width - 1 && _mapImage.GetPixel(temp.X + 1, y1) != targetColor)
                    {
                        spanRight = false;
                    }
                    y1++;
                }
            }
            return chunk;
        }

        /// <summary>
        /// Tries to get a point at the specified coordinates
        /// </summary>
        public Point TryGetPoint(int x, int y)
        {
            _mapPoints.TryGetValue(Tuple.Create(x, y), out Point point);
            return point;
        }
    }
}
