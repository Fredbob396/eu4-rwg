﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4Lib.Entities
{
    public class CultureGroup
    {
        public string Id { get; set; }
        public string Localisation { get; set; }

        public string GraphicalCulture { get; set; }

        public List<Culture> Cultures = new List<Culture>();

        public List<string> MaleNames { get; set; }
        public List<string> FemaleNames { get; set; }
        public List<string> DynastyNames { get; set; }
    }
}
