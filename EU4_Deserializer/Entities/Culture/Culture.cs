﻿using System.Collections.Generic;

namespace EU4Lib.Entities
{
    public class Culture
    {
        public string Id { get; set; }
        public string Localisation { get; set; }

        public Country Primary { get; set; }

        public List<string> MaleNames { get; set; }
        public List<string> FemaleNames { get; set; }
        public List<string> DynastyNames { get; set; }
    }
}
