﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4Lib.Entities
{
    class ReligionGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Province CenterOfReligion { get; set; }

        public bool DefenderOfFaith { get; set; }
        public bool CanFormPersonalUnions { get; set; }
        public bool AiWillPropagateThroughTrade { get; set; }

        public int FlagEmblemPercentage { get; set; }
        public int FlagEmblemIndexLower { get; set; }
        public int FlagEmblemIndexUpper { get; set; }

        public string HarmonizedModifier { get; set; }
        public string CrusadeName { get; set; }

        // TODO: Maybe add religious schools MUCH later
        // public List<ReligiousSchool> ReligiousSchools { get; set; }

        public List<Denomination> Denominations { get; set; }
    }
}
