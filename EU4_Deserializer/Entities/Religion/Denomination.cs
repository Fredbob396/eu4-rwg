﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4Lib.Entities
{
    // TODO: Decide on a way to handle the Papacy
    // TODO: Decide on how to handle aspects
    // TODO: Decide on how to handle religion bonuses
    // TODO: Decide on how to handle Orthodox Icons
    // TODO: Decide on how to handle Coptic Blessings
    class Denomination
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Color Color { get; set; }
        public int Icon { get; set; }

        public DateTime Date { get; set; }

        // Catholic
        public bool HreReligion { get; set; }

        // Protestant
        public bool HreHereticReligion { get; set; }

        // Protestant
        public bool UsesChurchPower { get; set; }

        // Reformed
        public bool Fervor { get; set; }

        // Anglican
        public bool UsesAnglicanPower { get; set; }

        // Protestant - Anglican - Reformed
        public List<Denomination> AllowedCenterConversion { get; set; }

        // Orthodox
        public bool HasPatriarchs { get; set; }

        // Coptic
        public List<Province> HolySites { get; set; }

        // Orthodox - Coptic
        public bool MisguidedHeretics { get; set; }

        // Sunni -Shiite - Ibadi
        public bool UsesPiety { get; set; }

        // Theravada - Vajrayana - Mahayana
        public bool UsesKarma { get; set; }

        // Confucianism
        public bool UsesHarmony { get; set; }

        // Shinto
        public bool UsesIsolationism { get; set; }

        // Hinduism - Norse
        public bool PersonalDiety { get; set; }

        // Fetishism
        public bool FetishistCult { get; set; }

        // Inti
        public bool Authority { get; set; }

        // Nahuatl
        public bool Doom { get; set; }
        public bool DeclareWarInRegency { get; set; }

        // Inti - Nahuatl - Mesoamerican
        public bool ReligiousReforms { get; set; }

        // Tengri
        public bool CanHaveSecondaryReligion { get; set; }

        public List<string> Heretics { get; set; }
        public List<Denomination> AllowedConversion { get; set; }
    }
}
