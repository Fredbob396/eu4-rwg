﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4Lib.Entities
{
    public class Definition
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public Color Color { get; set; }
    }
}
