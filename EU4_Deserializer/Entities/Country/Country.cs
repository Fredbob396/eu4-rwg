﻿using System.Collections.Generic;
using System.Web.UI;

namespace EU4Lib.Entities
{
    /// <summary>
    /// An EU4 Country
    /// </summary>
    public class Country
    {
        // Localisation
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Adjective { get; set; }

        // Common
        public string GraphicalCulture { get; set; }
        public Triplet Color { get; set; }
        public Triplet RevolutionaryColors { get; set; } // TODO: Get from Flag object!
        public List<string> HistoricalIdeaGroups { get; set; }
        public List<string> HistoricalUnits { get; set; }
        public List<MonarchName> MonarchNames { get; set; }
        public List<string> LeaderNames { get; set; }
        public List<string> ShipNames { get; set; }
        public List<string> ArmyNames { get; set; }
        public List<string> FleetNames { get; set; }

        // History
        // TODO: Figure out how I want to handle government stuff.
        // Loading directly from the EU4 file doesn't give me enough info
        // As annoying as a JSON dump is, it might be necessary...
        public string Government { get; set; }

        public Culture PrimaryCulture { get; set; }
        public List<Culture> AcceptedCultures { get; set; }

        public Religion Religion { get; set; }
        public Religion SecondaryReligion { get; set; }
        public List<Religion> HarmonizedReligions { get; set; }
        public string ReligiousSchool { get; set; }

        public TechnologyGroup TechnologyGroup { get; set; }

        public Province Capital { get; set; }
        public Province FixedCapital { get; set; }

        public List<Country> HistoricalRivals { get; set; }
        public List<Country> HistoricalFriends { get; set; }

        public double AddArmyProfessionalism { get; set; }
        public string NationalFocus { get; set; }

        public List<Monarch> Monarchs { get; set; }
    }
}
