﻿namespace EU4Lib.Entities
{
    /// <summary>
    /// Represents monarch_name in common/countries files
    /// </summary>
    public class MonarchName
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public int Chance { get; set; }
    }
}
