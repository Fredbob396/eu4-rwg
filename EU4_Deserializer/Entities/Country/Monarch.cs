﻿using System;

namespace EU4Lib.Entities
{
    /// <summary>
    /// Represents a monarch for a specific date in histroy/countries
    /// </summary>
    public class Monarch
    {
        public DateTime Date { get; set; }
        public bool Consort { get; set; } // AKA "Queen"
        public bool Female { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime DeathDate { get; set; }
        public string Name { get; set; }
        public string Dynasty { get; set; }
        public int Claim { get; set; }
        public int Adm { get; set; }
        public int Dip { get; set; }
        public int Mil { get; set; }
    }
}
