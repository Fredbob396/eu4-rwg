﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EU4Lib.Entities
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Color Color { get; set; }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }
}
