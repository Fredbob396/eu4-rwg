﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace EU4Lib.Entities
{
    // TODO: Handle province modifiers
    public class Province
    {
        public int Id { get; set; }
        public string DefinitionName { get; set; }
        public Color Color { get; set; }
        public bool Sea { get; set; }

        public Country Owner { get; set; }
        public Country Controller { get; set; }
        public List<Country> Cores { get; set; }

        public Culture Culture { get; set; }
        public Religion Religion { get; set; }

        public int BaseTax { get; set; }
        public int BaseProduction { get; set; }
        public int BaseManpower { get; set; }

        public int Unrest { get; set; }

        public bool IsCity { get; set; }
        public bool Fort15th { get; set; }
        public bool HRE { get; set; }

        public string Estate { get; set; }

        public int NativeSize { get; set; }
        public int NativeFerocity { get; set; }
        public int NativeHostileness { get; set; }

        // TODO: Investigate what this does
        public int ExtraCost { get; set; }

        public string TradeGoods { get; set; }
        public string Capital { get; set; }

        // TODO: Investigate some more
        public List<TechnologyGroup> DiscoveredBy { get; set; }

        public Area ParentArea { get; set; }
        public Region ParentRegion { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }

        public Dictionary<Tuple<int, int>, Point> MapPoints { get; set; }
        public List<Province> Neighbors = new List<Province>();

        public override string ToString()
        {
            return $"{DefinitionName} - {Id}";
        }
    }
}
