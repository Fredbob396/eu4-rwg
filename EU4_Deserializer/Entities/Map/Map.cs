﻿using System.Collections.Generic;
using System.Linq;

namespace EU4Lib.Entities
{
    public class Map
    {
        private readonly List<SuperRegion> _superRegions;

        public Map(List<SuperRegion> superRegions)
        {
            _superRegions = superRegions;
        }

        public List<SuperRegion> SuperRegions()
        {
            return _superRegions;
        }

        // Gets all regions on the map
        public List<Region> Regions()
        {
            return _superRegions.SelectMany(sr => sr.Regions()).ToList();
        }

        // Gets all areas on the map
        public List<Area> Areas()
        {
            return Regions().SelectMany(r => r.Areas()).ToList();
        }

        // Gets all provinces on the map
        public List<Province> Provinces()
        {
            return Areas().SelectMany(a => a.Provinces()).ToList();
        }
    }
}
