﻿using System.Collections.Generic;
using System.Linq;

namespace EU4Lib.Entities
{
    public class SuperRegion
    {
        private List<Region> _regions = new List<Region>();

        public string Name;
        public bool Sea { get; set; }
        public List<string> RegionNames = new List<string>();
        public List<SuperRegion> Neighbors = new List<SuperRegion>();

        public List<Region> Regions()
        {
            return _regions;
        }

        public List<Area> Area()
        {
            return _regions.SelectMany(r => r.Areas()).ToList();
        }

        public List<Province> Provinces()
        {
            return Area().SelectMany(a => a.Provinces()).ToList();
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
