﻿using System;
using System.Collections.Generic;

namespace EU4Lib.Entities
{
    public class Area
    {
        private List<Province> _provinces = new List<Province>();

        public string Name { get; set; }
        public bool Sea { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }
        public Region ParentRegion { get; set; }

        public List<string> ProvinceIds = new List<string>();
        public List<Area> Neighbors = new List<Area>();

        public List<Province> Provinces()
        {
            return _provinces;
        }

        public override string ToString()
        {
            return $"{Name}";
        }

        public Province GetRandomProvince(Random random)
        {
            var provIndex = random.Next(0, Provinces().Count + 1);
            return Provinces()[provIndex];
        }
    }
}
