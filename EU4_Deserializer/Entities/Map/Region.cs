﻿using System.Collections.Generic;
using System.Linq;

namespace EU4Lib.Entities
{
    public class Region
    {
        private List<Area> _areas = new List<Area>();

        public string Name { get; set; }
        public bool Sea { get; set; }
        public SuperRegion ParentSuperRegion { get; set; }

        public List<string> AreaNames = new List<string>();
        public List<Region> Neighbors = new List<Region>();

        public List<Area> Areas()
        {
            return _areas;
        }

        public List<Province> Provinces()
        {
            return _areas.SelectMany(a => a.Provinces()).ToList();
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
