﻿using System;
using EU4Lib.Entities;
using EU4Lib.MapService;

namespace EU4Lib
{
    public class Eu4Deserializer
    {
        public Eu4Deserializer(string eu4Directory)
        {
            G.Eu4Directory = eu4Directory;
        }

        /// <summary>
        /// Loads a blank version of the EU4 map 
        /// </summary>
        public Map LoadMap()
        {
            var mapDeserializer = new MapDeserializer();
            return mapDeserializer.GetDeserializedMap();
        }

        /// <summary>
        /// Loads all adjacencies for a given map
        /// </summary>
        public void LoadAllAdjacencies(Map map)
        {
            var adjacencyCalculator = new ProvinceAdjacencyCalculator(map);
            adjacencyCalculator.LoadAllAdjacencies();
        }

        /// <summary>
        /// Builds a database of every province's neighboring provinces.
        /// WARNING: Can take upwards of 30 minutes!
        /// </summary>
        public void BuildAdjacenciesDatabase(Map map)
        {
            var adjacencyCalculator = new ProvinceAdjacencyCalculator(map);
            adjacencyCalculator.BuildAdjacenciesDatabase();
        }
    }
}
